﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SupporTix.Models;

namespace SupporTix.ViewModels
{
    public class NewTicketViewModel
    {
        public IEnumerable<RequestType> RequestTypes { get; set; }
        public Ticket Ticket { get; set; }
    }
}