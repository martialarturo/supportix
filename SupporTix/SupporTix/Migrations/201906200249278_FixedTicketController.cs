namespace SupporTix.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixedTicketController : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Tickets", "Details", c => c.String(nullable: false, maxLength: 1000));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Tickets", "Details", c => c.String(nullable: false));
        }
    }
}
