namespace SupporTix.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateRequestTypes : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO RequestTypes (Name) VALUES ('Password Reset')");
            Sql("INSERT INTO RequestTypes (Name) VALUES ('Form/Application Update')");
            Sql("INSERT INTO RequestTypes (Name) VALUES ('New Drug or Drug Change')");
            Sql("INSERT INTO RequestTypes (Name) VALUES ('New Copay/Foundation')");
            Sql("INSERT INTO RequestTypes (Name) VALUES ('New Feature Request')");
            Sql("INSERT INTO RequestTypes (Name) VALUES ('Information on Drug Assistant PAP Software')");
            Sql("INSERT INTO RequestTypes (Name) VALUES ('Other')");
        }
        
        public override void Down()
        {
        }
    }
}
