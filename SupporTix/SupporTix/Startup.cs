﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SupporTix.Startup))]
namespace SupporTix
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
