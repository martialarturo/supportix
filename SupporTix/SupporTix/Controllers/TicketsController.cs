﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SupporTix.Models;
using SupporTix.ViewModels;

namespace SupporTix.Controllers
{
    public class TicketsController : Controller
    {
        private ApplicationDbContext _context;

        public TicketsController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        // GET: Tickets
        public ActionResult Index()
        {
            return View();
        }

        //GET: NewTicket
        public ActionResult NewTicket()
        {
            var requestTypes = _context.RequestTypes.ToList();
            var viewModel = new NewTicketViewModel
            {
                RequestTypes = requestTypes
            };

            return View(viewModel);
        }

        //POST: Create
        public ActionResult Create(Ticket ticket)
        {
            _context.Tickets.Add(ticket);
            _context.SaveChanges();

            return RedirectToAction("Index", "Home");
        }
    }
}